# Tile Windows


## DESCRIPTION

A wrapper over [`wmctrl`][1],
to use as keyboard shortcut script
for tiling windows in the X11 Desktop Environment.

## SYNOPSIS

    perl tile_windows.pl <width> <height> <column_index> <row_index>

The screen is divided into a 3x3 matrix.
The top left corner has `row_index`=0, and `column_index`=0.
The top right corner has `row_index`=0, and `column_index`=2.
`width`/`height` are number of columns/rows to fill.


## USEFUL COMMANDS

    xwininfo 
    wmctrl -p -G -l 
    wmctrl -d
    perl -e 'require "tile_windows.pl"; move_resize_current_win(qw/0 1 2 1/)'
    perl -e 'require "tile_windows.pl"; position_current_win(numpad_to_position(8))'
    perl test.plx


[1]: http://tripie.sweb.cz/utils/wmctrl/

