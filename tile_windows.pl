# wmctrl -r :ACTIVE: -e 0,50,50,1000,500
# list of states i want to consider.
use strict;
use warnings;
use List::Util qw(all);

sub readable_to_internal {
    map { int ($_ -1) } @_;
}

sub meaningful_geometry {
    my ($w, $h, $i, $j) = @_;
    my %allowed_offset = map { $_ => 1 } qw(0 0.5 1 2);
    my %allowed_length = map { $_ => 1 } qw(0 1 2);
    (($i + $w) >= 0)
    && (($i +$w) <=2)
    && (($j +$h) >=0)
    && (($j +$h) <=2)
    && (all { $allowed_offset{$_} } $i, $j)
    && (all { $allowed_length{$_} } $w, $h)
    && ($i != 0.5 || $w == 1)
    && ($j != 0.5 || $h == 1)
}

sub sample {
    my $a = pop;
    return @_
      ? map {
        my $i = $_;
        map { [ @$i, $_ ] } @$a
      } sample(@_)
      : map { [$_] } @$a;
}

sub generate_meaningful_geometries {
    grep { meaningful_geometry @$_ } sample ( ([0..2]) x 2, ([0,0.5,1,2]) x 2);
}

#print join(' ', @$_), "\n" for generate_meaningful_args;

sub current_display {
    open( my $desktops, "wmctrl -d|" );
    map {
        /DG:\ (\d+)x(\d+).*
      WA:\ (\d+),(\d+)\ (\d+)x(\d+)/x
      }
      grep { /\*/ } <$desktops>;
}

sub active_window {
    open( my $activewindow, "xdotool getwindowfocus getwindowgeometry|" );
    ( join "", map { chomp; $_ } <$activewindow> ) =~ /Window\ +(\d+)\ +
    Position:\ (\d+),(\d+)\ \(screen:\ 0\)\ +
    Geometry:\ (\d+)x(\d+)/x;

}

sub move_resize_current_win {
    my ( $w, $h, $i, $j ) = @_;
    $h++;
    $w++;

    my ( $full_width, $full_height, $originx, $originy, $width, $height ) =
      current_display;

    my $title_bar_height = $full_height - $height;
    $height -= $title_bar_height;

    my ( $gutter_width, $gutter_height ) =
      map { int( $_ / 3 ) } ( $width, $height );

    my $cmd = sprintf(
        "wmctrl -r :ACTIVE: -e 0,%d,%d,%d,%d",
        int($originx + $i * $gutter_width),
        int($title_bar_height + $j * $gutter_height),
        $gutter_width * $w,
        $gutter_height * $h
    );
    system($cmd );
    print STDERR $cmd, " @_\n";
}

#move_resize_current_win @ARGV;
sub position_current_win {
    my $loc = shift;
    move_resize_current_win(
        1, 1,
        int( ( $loc - 1 ) % 3 ) + 1,
        int( ( $loc - 1 ) / 3 ) + 1
    );
}

sub numpad_to_position {
    my $num = shift;
    if    ( $num > 6 ) { $num - 6 }
    elsif ( $num < 4 ) { $num + 6 }
    else               { $num }
}

sub move_current_win {
  my $loc = shift;
  my (%win, %display);
  @win{ qw(id originx originy width height) } =
    active_window;

    @display{qw(full_width full_height originx originy width height)} =
      current_display;

    $display{title_bar_height} = $display{full_height} - $display{height};
    my $usable_height = $display{height} - $display{title_bar_height};

    my $cmd = sprintf(
        "wmctrl -i -r %d -e 0,%d,%d,%d,%d",
        $win{id},
        $display{originx} + int ($loc % 3) * ($display{width}-$win{width})/2,
        $display{title_bar_height}  + int ($loc / 3) * ($usable_height-$win{height})/2,
        $win{width},
        $win{height}
    );

    system($cmd);
    print STDERR $cmd, "\n";
}

sub toggle_length_x {
    #TODO retrieve w x i j from current actual window stuff
    #TODO then, here, you want to increase w by one unit, modulo 3
    #TODO (might want to add decrease feature later)
    #TODO and subtract i times 0.5 from x
    #TODO return w x i j
    my ( $w, $h, $i, $j ) = @_;
    my ( $full_width, $full_height, $originx, $originy, $width, $height ) = current_display;
    my $title_bar_height = $full_height - $height;
    $height -= $title_bar_height;
    my ( $gutter_width, $gutter_height ) =
      map { int( $_ / 3 ) } ( $width, $height );
}

sub toggle_length_y {
    #TODO retrieve w x i j from current actual window stuff
    #TODO then, here, you want to increase h by one unit, modulo 3
    #TODO (might want to add decrease feature later)
    #TODO and subtract j times 0.5 from y
    #TODO return w x i j
    my ( $w, $h, $i, $j ) = @_;
    my ( $full_width, $full_height, $originx, $originy, $width, $height ) = current_display;
    my $title_bar_height = $full_height - $height;
    $height -= $title_bar_height;
    my ( $gutter_width, $gutter_height ) =
      map { int( $_ / 3 ) } ( $width, $height );
}

#TODO add keyboard shortcuts. C-M-<NUMPADNUM> for positioning. with w = h = 0
#TODO add keyboard shortcuts. C-M-<whatever> for toggling w.
#TODO add keyboard shortcuts. C-M-<whatever> for toggling h.
1;
